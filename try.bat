@echo off

IF %1==this GOTO entro
IF %1==konrradf GOTO konrradf
IF %1==Wkonrradf GOTO Wkonrradf
IF %1==pf GOTO protectFiles
IF %1==public GOTO konrradf
IF %1==arriendaya GOTO konrradf
IF %1==Wavaluosya GOTO Wavaluosya
IF %1==avaluosya GOTO avaluosya
IF %1==todo_konrradf GOTO konrradf
ECHO -          sitio u opción: %1 no encontrado
GOTO end

:konrradf
cd C:/kvn/server/konrradf/
GOTO entro

:Wkonrradf
cd C:/kvn/server/W/konrradf/
GOTO entro

:protectFiles
cd C:/kvn/server/protectFiles/
GOTO entro

:avaluosya
cd C:/kvn/server/aval/
GOTO entro

:Wavaluosya
cd C:/kvn/server/W/avaluosya/
GOTO entro

:entro
IF %2==status GOTO status
IF %2==checkout GOTO checkout
IF %2==open GOTO open
IF %2==branch GOTO branch
IF %2==lasttag GOTO lasttag
IF %2==newtag GOTO newtag
IF %2==c GOTO commit
IF %2==log GOTO log
IF %2==push GOTO push
IF %2==pull GOTO pull
ECHO -          Funcion %2 no encontrada
GOTO end

:status
ECHO -          revisar estado
git status
GOTO end

:checkout
ECHO -          cambiando de rama a %3
git checkout %3
GOTO end

:branch
ECHO -          revisando ramas
git branch
GOTO end

:lasttag
ECHO -          leyendo ultimo tag
git for-each-ref refs/tags --sort=-taggerdate --count=1
GOTO end

:newtag
ECHO -          CREAR TAG
ECHO -          push al repositorio %1
git push
ECHO -          creando tag %3
git tag -a %3 -m %4
ECHO -          subiendo tag %3
git push origin %3
GOTO end

:commit
ECHO -          Guardando cambios
git add .
git commit -m %3
GOTO end

:log
ECHO -          leyendo ultimos %3 commit
git log -%3
GOTO end

:push
ECHO -          push al repositorio
git push
GOTO end

:pull
ECHO -          pull al repositorio
git pull
GOTO end

:open
ECHO -          abriendo sitio %1
start powershell

:end
exit